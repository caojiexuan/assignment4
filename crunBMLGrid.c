
#include <stdio.h>
//#include <R.h>
#include <string.h>



//move cars together, blue = 3 red = 2 blank = 1
void crunBMLGrid(int *grid, int *numSteps, int *row, int *col, int *nextgrid, int *movedCars, int *blockedCars, int *nblue, int *nred){
    
    int i, j, k, cur_pos, next_pos;
    
    
    
    for ( k = 0; k < *numSteps; k++) {
        int n = 0;//save the movedcars number
        
        //move blue cars
        if (k % 2 == 0){
            
            for ( j = 0; j < *col; j++ ){
                for ( i = 0; i < *row; i++ ){
                    cur_pos = j*(*row) + i;
                    if (grid[cur_pos] == 3){
                        if (i == *row - 1) next_pos = j*(*row);
                        else next_pos = j*(*row) + i + 1;
                        if (grid[next_pos] == 1){
                            nextgrid[next_pos] = 3;
                            nextgrid[cur_pos] = 1;
                            n = n + 1;
                        }
                    }
                }
            }
            //copy the new grid to the old grid to do the next step
            memcpy( grid, nextgrid, sizeof( int )* (*row) * (*col) );
            blockedCars[k] = *nblue - n;
        }
        
        //move red cars
        else{
            for ( j = 0; j < *col; j++ ){
                for ( i = 0; i < *row; i++ ){
                    cur_pos = j**row+i;
                    if (grid[cur_pos] == 2){
                        if (j == *col - 1) next_pos = i;
                        else next_pos = (j + 1)*(*row) + i;
                        if (grid[next_pos] == 1){
                            nextgrid[next_pos] = 2;
                            nextgrid[cur_pos] = 1;
                            n = n + 1;
                        }
                    }
                }
            }
            //copy the new grid to the old grid to do the next step
            memcpy( grid, nextgrid, sizeof( int )* (*row) * (*col) );
            blockedCars[k] = *nred - n;
        }
        movedCars[k] = n;
    }
    
}
